/***** DEBUG *****/

var bgi = 1;

function bg() {
	if(bgi == 0) {
		$('body').css("background-image", "url('images/room/background.jpg')");
		bgi = 1;
	} else if(bgi == 1) {
		$('body').css("background-image", "url('images/room/background-tastycat.jpg')");
		bgi = 2;
	} else {
		$('body').css("background-image", "url('images/room/background-lounge.jpg')");
		bgi = 0;
	}
}

function light() {
	document.getElementById("light").start();
}

function load() {
	var params = { allowScriptAccess: "always" };
	var atts = { id: "flash" };
	swfobject.embedSWF("http://www.youtube.com/v/rjRLpwAe8Kk?enablejsapi=1&playerapiid=ytplayer&version=3",
	               	   "flash", "484", "271", "8", null, null, params, atts);
}

/*****************/

/** DO NO TOUCH **/

(function () {
    var a = !1,
        b = /xyz/.test(function () {
            xyz
        }) ? /\b_super\b/ : /.*/;
    this.Class = function () {};
    Class.extend = function (c) {
        function d() {
            !a && this.init && this.init.apply(this, arguments)
        }
        var e = this.prototype;
        a = !0;
        var f = new this;
        a = !1;
        for (var g in c) f[g] = "function" == typeof c[g] && "function" == typeof e[g] && b.test(c[g]) ? function (a, b) {
            return function () {
                var c = this._super;
                this._super = e[a];
                var d = b.apply(this, arguments);
                this._super = c;
                return d
            }
        }(g, c[g]) : c[g];
        d.prototype = f;
        d.prototype.constructor = d;
        d.extend = arguments.callee;
        return d
    }
})();

/*****************/

/***** CLASS *****/

var Room = Class.extend({
	init: function () {
		this.myUsername = null;
	},

	getMyUsername: function() {
		return this.myUsername;
	},

	setMyUsername: function(username) {
		this.myUsername = username;
	}
});

var Player = Class.extend({
	init: function () {
		this.youtubePlayer = null;
		this.isReady = false;
	},

	setYoutubePlayer: function(ytplayer) {
		this.youtubePlayer = ytplayer;
		this.isReady = true;
	},

	play: function() {
		if(this.isReady) {
			this.youtubePlayer.playVideo();
		}
	},

	pause: function() {
		if(this.isReady) {
			this.youtubePlayer.pauseVideo();
		}
	},

	stop: function() {
		if(this.isReady) {
			this.youtubePlayer.stopVideo();
		}
	},

	mute: function() {
		if(this.isReady) {
			if(this.youtubePlayer.isMuted()) {
				this.youtubePlayer.unMute();
			} else {
				this.youtubePlayer.mute();
			}
		}
	},
});

var Chat = Class.extend({
	init: function () {
		this.chatInput = $("#chat-input").focus($.proxy(this.onChatFocus, this)).blur($.proxy(this.onChatBlur, this))[0];
		this.chatMessages = $("#chat-messages");
		this.chatButtonSend = $("#chat-send").click($.proxy(this.onClickSendButton, this));
	},

	onChatReceived: function(a) {
		var author = a.author, message = a.message, b = $("<div/>").addClass("chat-" + a.type), time = a.time;
		
		b.append($("<div/>").addClass("chat-timestamp").text(time));
		b.append($("<span/>").addClass("chat-username").text(author));
		b.append($("<span/>").addClass("chat-text").text(": " + message));

		this.chatMessages.append(b);
		this.chatMessages.scrollTop(this.chatMessages[0].scrollHeight);
	},

	onChatFocus: function () {
        this.chatInput.placeholder = "";
    },

    onChatBlur: function () {
        this.chatInput.placeholder = "Clique ici pour te joindre à la conversation";
    },

    onChatClear: function() {
        this.chatMessages.html("").scrollTop(0);
    },

    onKeyPress: function(obj, event) {
    	var key, c = {};

        if (window.event)
            key = window.event.keyCode;
        else if (event)
            key = event.which;
        else
            return !0;

        if(key == 13 || key == 9) {
        	this.onSend();
        }

        return !0;
    },

    onClickSendButton: function() {
    	this.onSend();
    },

    onSend: function() {
    	if(this.chatInput.value != "" && this.chatInput.value.length > 0) {
			var c = {}

	    	c.author = room.getMyUsername();
	    	c.message = this.chatInput.value;
	    	c.type = "message";
	    	c.time = this.getCurrentChatTime();

	    	this.onChatReceived(c);
	    	this.chatInput.value = "";
    	}
    },

    getCurrentChatTime: function() {
		var now = new Date()
		return zero(now.getHours()) +':'+ zero(now.getMinutes());
	},
});

/*****************/

function onDocumentReady() {
	
}

function onYouTubePlayerReady(playerId) {
	player.setYoutubePlayer(document.getElementById("flash"));
}

function zero(n) {
	return ("0" + n).slice(-2);
}

/*****************/

room = new Room;
player = new Player;
chat = new Chat;

room.setMyUsername("Yann");